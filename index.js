const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());


/**
 ** 2.1. Добавить в роут POST /post проверку на наличие Header: Key (на уровне middleware), если такого header не существует, то возвращать 401
 */

const checkHeaderKey = (req, res, next) => {
	(req.header('Key') === undefined) ? res.sendStatus(401) : next();
}


/**
 **	1.1. GET / – Главная страница которая вернет код 200 OK и покажет текст "Hello, Express.js"
 */

app.get('/', (req, res) => {
	res.send('Hello, Express.js');
});


/**
 ** 1.2 GET /hello – Страница, код ответа 200 OK и покажет текст "Hello stranger!"
 */

app.get('/hello', (req, res) => {
	res.send('Hello stranger!');
});


/**
 ** 1.3 GET /hello/[любое имя] – Страница, код ответа 200 OK и покажет текст "Hello, [любое имя]!"
 */

app.get('/hello/:name', (req, res) => {
	res.send(`Hello ${req.params.name}!`);
});


/**
 ** ANY /sub/[что угодно]/[возможно даже так] – Любая из этих страниц должна показать текст "You requested URI: [полный URI запроса]"
 */

app.all('/sub/**', (req, res) => {
	res.send(`You requester URI: ${req.protocol}://${req.get('host')}${req.originalUrl}`);
});


/**
 ** POST /post – Страница которая вернет все тело POST запроса (POST body) в JSON формате, либо 404 Not Found - если нет тела запроса
 */

app.post('/post', checkHeaderKey, (req, res) => {
	(req.body.constructor === Object && Object.keys(req.body).length === 0) ? res.sendStatus(404) : res.send(req.body);
});

app.listen(3000, () => {
	console.log('Server started on port 3000.');
});